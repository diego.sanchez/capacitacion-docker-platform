# Curso docker

Aquí la [presentación](https://docs.google.com/presentation/d/1Cujn82SIpr7bAZJ47LDyVrNEL1NXaSJk2izJEUr35A4/edit?usp=sharing)


## Herramientas recomendadas:

| Tool        | Version | Instalacion          |
|-------------|---------|----------------------|
| `jq`        | `1.6`   | `brew install jq`    |
| `redis-cli` | `5.0.3` | `brew install redis` |
|             |         |                      |

## Utilización de imagenes existentes

### Redis
Descargamos la imagen que vamos a utilizar:

```bash
docker pull redis:6.0.6
```

Ejecutamos el contenedor:

```bash
docker run -d --name redis redis:6.0.6 
```

Verificamos si el contenedor se está ejecutando:

```bash
docker container ls
```

Verificamos si nos podemos conectar al servicio:

```bas
redis-cli
```

Vinculamos puertos y volumenes:

```bash
mkdir -p $HOME/tmp/redis_data
docker run -d --name redis-persistent -v /Users/diego/tmp/redis_data:/data -p 6379:6379 redis:6.0.6
```

Verificamos puntos de mentaje:

```bash
docker container inspect redis-persistent | jq .[].Mounts
```

```bash
docker container inspect redis-persisten | jq .[].State
```

#### Cleanup:

```bash
rm -Rfv $HOME/tmp/redis_data
```

## Creación de imagenes 

### Missions

```bash
docker login docker.etermax.net -u <user-id> -p pwd*
```

*Respecto a las cuentas con 2FA, loguearse a la registry requiere un [token personal](https://gitlab.etermax.net/help/user/profile/account/two_factor_authentication#personal-access-tokens)

Construccion de imagen

```bash
docker build -t missions:<tag-id>
```

Vinculamos puertos y volumenes:

```bash
docker run \
        -e "AWS_ACCESS_KEY_ID=<aws_access_key>" \
        -e "AWS_SECRET_ACCESS_KEY=<aws_secret_key>" \
        -e "USER_MISSION_TABLE_NAME=staging_user_mission" \
        -e "MISSIONS_DEFINITIONS_TABLE_NAME=staging_missions_definitions" \
        -e "DYNAMO_AWS_DEFAULT_REGION=us-east-1" \ 
        -e "WEBHOOKS_TABLE_NAME=staging_missions_webhooks" \
        -e "ENABLE_NEWRELIC=false" \
        -p 8080:8080 
        -d \
        missions:<tag-id>
```

#### Cleanup:

```bash
docker container stop <name>
```

```bash
docker container prune
```

```bash
docker image prune
```

